/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/editor.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/editor.js":
/*!**************************!*\
  !*** ./assets/editor.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);


var __ = wp.i18n.__;
var registerPlugin = wp.plugins.registerPlugin;
var Fragment = wp.element.Fragment;
var _wp$components = wp.components,
    PanelBody = _wp$components.PanelBody,
    PanelRow = _wp$components.PanelRow,
    SelectControl = _wp$components.SelectControl,
    TextControl = _wp$components.TextControl,
    CheckboxControl = _wp$components.CheckboxControl;
var PluginSidebar = wp.editPost.PluginSidebar;
var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    withDispatch = _wp$data.withDispatch,
    select = _wp$data.select;
var compose = wp.compose.compose;
var domReady = wp.domReady;
var posts = []; //@link https://rudrastyh.com/gutenberg/plugin-sidebars.html
//@link https://rsvpmaker.com/blog/2019/12/26/gutenberg-sidebar-howto/

var PluginSidebarTest = function PluginSidebarTest() {
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(Fragment, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(PluginSidebar, {
    name: "plugin-sidebar-test",
    title: __('Add a WP Popup', 'wp-popup'),
    icon: "slides"
  }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(PanelBody, {
    title: __('Settings', 'wp-popup')
  }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(PanelRow, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(PopupSelect, {
    label: __('Select a Popup to display on this page.', 'wp-popup'),
    metaKey: "wp_popup_display_lightbox"
  })), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(PanelRow, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(StaticDropDown, {
    label: __('Once Seen', 'wp-popup'),
    metaKey: "wp_popup_suppress",
    selectOptions: [{
      value: 'always',
      label: __('Never show it to that user again', 'wp-popup')
    }, {
      value: 'session',
      label: __('Don\'t show again during the user\'s current browser session', 'wp-popup')
    }, {
      value: 'wait-7',
      label: __('Wait a week before showing it again', 'wp-popup')
    }, {
      value: 'wait-30',
      label: __('Wait 30 days before showing it again', 'wp-popup')
    }, {
      value: 'wait-90',
      label: __('Wait 90 days before showing it again', 'wp-popup')
    }, {
      value: 'never',
      label: __('Keep showing it', 'wp-popup')
    }],
    help: __('What should happen after a user sees this popup? Note: This setting may be overridden when a user clears their cookies.', 'wp-popup')
  })), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(PanelRow, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(StaticDropDown, {
    label: __('Trigger', 'wp-popup'),
    metaKey: "wp_popup_trigger",
    selectOptions: [{
      value: 'immediate',
      label: __('Immediately on page load', 'wp-popup')
    }, {
      value: 'delay',
      label: __('N seconds after load (specify)', 'wp-popup')
    }, {
      value: 'scroll',
      label: __('After page is scrolled N pixels (specify)', 'wp-popup')
    }, {
      value: 'scroll-half',
      label: __('After page is scrolled halfway', 'wp-popup')
    }, {
      value: 'scroll-full',
      label: __('After page is scrolled to bottom', 'wp-popup')
    }, {
      value: 'minutes',
      label: __('After N minutes spent on site this visit (specify)', 'wp-popup')
    }, {
      value: 'pages',
      label: __('Once N pages have been visited in last 90 days (specify)', 'wp-popup')
    }],
    help: __('When does the popup appear?', 'wp-popup')
  })), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(PanelRow, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(TriggerAmount, {
    label: __('Trigger Amount', 'wp-popup'),
    metaKey: "wp_popup_trigger_amount",
    help: __('Specify the precise quantity/time/amount/number ("N") for the trigger.', 'wp-popup')
  })), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(PanelRow, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(DisableOnMobile, {
    label: __('Disable on Mobile', 'wp-popup'),
    metaKey: "wp_popup_disable_on_mobile",
    help: __('Check this box to suppress this popup on mobile devices. (Recommended)', 'wp-popup')
  })))));
};

var DisableOnMobile = compose(withDispatch(function (dispatch, props) {
  return {
    setMetaValue: function setMetaValue(metaValue) {
      dispatch('core/editor').editPost({
        meta: _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, props.metaKey, metaValue)
      });
    }
  };
}), withSelect(function (select, props) {
  return {
    metaValue: select('core/editor').getEditedPostAttribute('meta')[props.metaKey]
  };
}))(function (props) {
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(CheckboxControl, {
    label: props.label,
    value: props.metaValue,
    checked: props.metaValue,
    onChange: function onChange(content) {
      return props.setMetaValue(content);
    },
    help: props.help
  });
});
var TriggerAmount = compose(withDispatch(function (dispatch, props) {
  return {
    setMetaValue: function setMetaValue(metaValue) {
      dispatch('core/editor').editPost({
        meta: _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, props.metaKey, metaValue)
      });
    }
  };
}), withSelect(function (select, props) {
  return {
    metaValue: select('core/editor').getEditedPostAttribute('meta')[props.metaKey]
  };
}))(function (props) {
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(TextControl, {
    label: props.label,
    value: props.metaValue,
    onChange: function onChange(content) {
      return props.setMetaValue(content);
    },
    help: props.help
  });
});
var StaticDropDown = compose(withDispatch(function (dispatch, props) {
  return {
    setMetaValue: function setMetaValue(metaValue) {
      dispatch('core/editor').editPost({
        meta: _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, props.metaKey, metaValue)
      });
    }
  };
}), withSelect(function (select, props) {
  return {
    metaValue: select('core/editor').getEditedPostAttribute('meta')[props.metaKey]
  };
}))(function (props) {
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(SelectControl, {
    label: props.label,
    value: props.metaValue,
    options: props.selectOptions,
    onChange: function onChange(content) {
      return props.setMetaValue(content);
    },
    help: props.help
  });
});
var PopupSelect = compose(withDispatch(function (dispatch, props) {
  return {
    setMetaValue: function setMetaValue(metaValue) {
      dispatch('core/editor').editPost({
        meta: _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, props.metaKey, metaValue)
      });
    }
  };
}), withSelect(function (select, props) {
  {
    /*  withSelect( returns all the WP Popup posts and meta for the current post )( uses the WP Popup posts to display a select and save that select as post meta  ) */
  }
  return {
    posts: select('core').getEntityRecords('postType', 'wp_popup', {
      per_page: -1
    }),
    metaValue: select('core/editor').getEditedPostAttribute('meta')[props.metaKey]
  };
}))(function (props) {
  var options = [];

  if (props.posts) {
    options.push({
      value: 0,
      label: __('Select a WP Popup', 'wp-popup')
    });
    props.posts.forEach(function (post) {
      options.push({
        value: post.id,
        label: post.title.rendered
      });
    });
  } else {
    options.push({
      value: 0,
      label: __('Loading...', 'wp-popup')
    });
  }

  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["createElement"])(SelectControl, {
    label: props.label,
    value: props.metaValue,
    options: options,
    onChange: function onChange(content) {
      return props.setMetaValue(content);
    }
  });
});
domReady(function () {
  if (select('core/editor').getCurrentPostType() === 'wp_popup') return;
  registerPlugin('plugin-sidebar-test', {
    render: PluginSidebarTest
  });
});

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/defineProperty.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/defineProperty.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

module.exports = _defineProperty;

/***/ }),

/***/ "@wordpress/element":
/*!******************************************!*\
  !*** external {"this":["wp","element"]} ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["element"]; }());

/***/ })

/******/ });
//# sourceMappingURL=editor.js.map